﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
namespace Answer485
{
    public partial class Form1 : Form
    {
       
        int LNMIN = 1;
        int LNMAX = 100;

        int VALUEMIN = 1;
        int VALUEMAX = 255;

        public static object locker = new object();
        public Form1()
        {
            InitializeComponent();
            //for (int i = 0; i < 20; i++)
            //{
            //    comobj[i] = new Com();
            //    comobj[i].frm = this;
            //    comser[i] = new ComSer();
            //    comser[i].frm = this;
            //}
        }
        //byte[] port_clear = new byte[1];
        static uint MAXCOM = 20;
        Com [] comobj = new Com [MAXCOM];
        ComSer[] comser = new ComSer[MAXCOM];



        private void Form1_Load(object sender, EventArgs e)
        {
            //XmlSerializer formatter = new XmlSerializer(typeof(Com));

            for (int i = 0; i < MAXCOM; i++)
            {
                comobj[i] = new Com();
                comobj[i].frm = this;
                comser[i] = new ComSer();
                //comser[i].frm = this;
            }
            //XmlSerializer formatter = new XmlSerializer(typeof(Com));
            Init();
            //XmlSerializer formatter = new XmlSerializer(typeof(Com));
            for (uint chanel = 1; chanel < (MAXCOM + 1); chanel++)
            {
                string buildnote = "com";
                string buildchanel = chanel.ToString();
                string buildend = "r.xml";
                string buildsum = buildnote + buildchanel + buildend;

                XmlSerializer formatter = new XmlSerializer(typeof(ComSer));

                // десериализация
                //using (FileStream fstream = new FileStream(@buildsum, FileMode.Create))//@"E:\note.txt"
                using (FileStream fs = new FileStream(@buildsum, FileMode.OpenOrCreate))
                {
                    try
                    {
                        comser[chanel - 1] = (ComSer)formatter.Deserialize(fs);

                        //Console.WriteLine("Имя: {0} --- Возраст: {1}", newPerson.Name, newPerson.Baudrate);
                        if (!comobj[chanel - 1].serialPort.IsOpen)
                        {
                            comobj[chanel - 1].serialPort.PortName = comser[chanel - 1].Name;
                            comobj[chanel - 1].serialPort.BaudRate = comser[chanel - 1].Baudrate;//comboBox1.SelectedItem.ToString();
                            comobj[chanel - 1].serialPort.Open();
                            //Com1PortExist = 1;
                            comobj[chanel - 1].labelName.Text = comobj[chanel - 1].serialPort.PortName;
                            comobj[chanel - 1].labelBaudrate.Text = comobj[chanel - 1].serialPort.BaudRate.ToString();
                            comobj[chanel - 1].comboBox.Text = comobj[chanel - 1].serialPort.BaudRate.ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show("Настройте конфигурацию");
                        //Close();
                        comobj[chanel - 1].groupBox.BackColor = Color.LightPink;
                    }

                }
            }

       
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


            string[] str = System.IO.Ports.SerialPort.GetPortNames();
            for (int i=0; i< MAXCOM; i++)
            {
                try
                {
                    //string[] str = System.IO.Ports.SerialPort.GetPortNames();
                   
                    comobj[i].listBox.Items.AddRange(str);
                    comobj[i].listBox.SetSelected(0, true);
                    comobj[i].listBox.SelectedItem = comobj[i].serialPort.PortName;
                }
                catch (ArgumentOutOfRangeException)
                {
                    MessageBox.Show("Подключите устройство к порту 1 (нет ком порта)");
                    Close();
                }

            }


     
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            for (uint i = 0; i < MAXCOM; i++)
            {
                if (comobj[i].serialPort.IsOpen)
                {
                    var pollingThread = new Thread(comobj[i].Polling);
                    pollingThread.IsBackground = true;
                    pollingThread.Start();
                }
            }

 
        }

        public void Func_Polling(Mutex mtxcom, System.IO.Ports.SerialPort serialPort, AutoResetEvent are, Label label1,
            Label label2, Label label3, Label label4, GroupBox groupBox)
        {
            long b = 0;
            int sent = 0;
            int received = 0;
            byte[] port_clear = new byte[1];
            //Thread.Sleep(3000);



            //serialPort.Write(port_clear, 0, 1);
            //Thread.Sleep(1000);
            //mtxcom.WaitOne();
            //while (serialPort.BytesToRead != 0)
            //{

            //    byte[] data = new byte[serialPort.BytesToRead];
            //    serialPort.Read(data, 0, data.Length);
            //    // Array.Copy(data, 0, InBuffer, Count, data.Length);
            //    // Count += data.Length;
            //    Thread.Sleep(10);
            //}

            //mtxcom.ReleaseMutex();
            //Thread.Sleep(1000);

            //mtxcom.WaitOne();
            //while (serialPort.BytesToRead != 0)
            //{

            //    byte[] data = new byte[serialPort.BytesToRead];
            //    serialPort.Read(data, 0, data.Length);

            //    Thread.Sleep(10);
            //}

            //mtxcom.ReleaseMutex();

            while (true)
            {
                if (serialPort.IsOpen)
                {

                    byte[] MidBuffer = new byte[1024];
                    byte[] InBuffer = new byte[1024];
                    Random rnd = new Random();
                    Random rndln = new Random();

                    int obj = rnd.Next(VALUEMIN, VALUEMAX);
                    int objln = rndln.Next(LNMIN, LNMAX);

                    for (int i = 0; i < objln; i++)
                        InBuffer[i] = (byte)obj;

                    int Count = objln;
                    are.WaitOne();
                    Stopwatch SW = new Stopwatch(); // Создаем объект

                    //mtxcom.WaitOne();
                    //try //---зчищаем мусор------------------------
                    //{
                    //    serialPort.ReadTimeout = 1;
                    //    byte[] data = new byte[1024];

                    //    for (int i = 1; i < 1024; i++)
                    //    {
                    //        InBuffer[i] = (byte)serialPort.ReadByte();
                    //    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    serialPort.ReadTimeout = 30;
                    //}
                    //mtxcom.ReleaseMutex();

                    Thread.Sleep(20);
                    SW.Start();
                    try
                    {

                        //mtxcom1.WaitOne();

                        serialPort.Write(InBuffer, 0, objln);
                        //mtxcom1.ReleaseMutex();
                    }                   
                    catch (Exception ex)
                    {
                        continue;
                    }

                    try //---зчищаем мусор------------------------
                    {
                        serialPort.ReadTimeout = 1;
                        byte[] data = new byte[1024];

                        for (int i = 1; i < 1024; i++)
                        {
                            InBuffer[i] = (byte)serialPort.ReadByte();
                        }
                    }
                    catch (Exception ex)
                    {
                        serialPort.ReadTimeout = 30;
                    }



                    sent++;
                    label1.Invoke(new Action<String>(t => label1.Text = t), Convert.ToString(sent));
                    Array.Clear(InBuffer, 0, 1024);
                    Count = 0;
                    try
                    {
                        serialPort.ReadTimeout = 5000;
                        InBuffer[0] = (byte)serialPort.ReadByte();
                        Thread.Sleep(1);
                    }
                    catch (Exception ex)
                    {
                        serialPort.ReadTimeout = 5000;
                        Thread.Sleep(500);
                        continue;
                    }
                    Count++;
                


                    mtxcom.WaitOne();
              
                    long a = 0;
                  // Stopwatch SW = new Stopwatch(); // Создаем объект
                       
                    try
                    {
                        serialPort.ReadTimeout = 30;
                        byte[] data = new byte[1024];
                        
                        for (int i = 1; i < 1024; i++)
                        {
                            //SW.Reset();
                            //SW.Start();
                            InBuffer[i] = (byte)serialPort.ReadByte();
                            Count++;
                          
                        }
                    }
                    catch (Exception ex)
                    {
                        serialPort.ReadTimeout = 30;              
                    }

                    mtxcom.ReleaseMutex();
                    label4.Invoke(new Action<String>(t => label4.Text = t), Convert.ToString(b));//максимальное значение

                    SW.Stop(); //Останавливаем
                    received++;
                    label2.Invoke(new Action<String>(t => label2.Text = t), Convert.ToString(received));
                  
                    label3.Invoke(new Action<String>(t => label3.Text = t), Convert.ToString(SW.ElapsedMilliseconds));

                     a = SW.ElapsedMilliseconds;
                    if (a > b)
                    {
                        b = a;
                        label4.Invoke(new Action<String>(t => label4.Text = t), Convert.ToString(b));
                    }


                    if (objln != Count)
                    {
                        groupBox.Invoke(new Action<Color>(t => groupBox.BackColor = t), Color.LightGreen);
                        lock (Form1.locker)
                        {
                            // запись в файл
                            string buildnote = "otherlength";
                            //string buildchanel = chanel.ToString();
                            string buildend = ".txt";
                            string buildsum = buildnote + buildend;

                            StreamWriter sw;
                            sw = File.AppendText(buildsum);
                            sw.WriteLine(Count.ToString() + "   " + serialPort.PortName);
                            sw.Close();
                        }
                        Thread.Sleep(1);
                        continue;
                    }
                    else
                    {
                        groupBox.Invoke(new Action<Color>(t => groupBox.BackColor = t), Color.LightGray);
                    }

                    for (int i = 0; i < objln; i++)
                    {
                        if (InBuffer[i] != obj)
                        {
                            groupBox.Invoke(new Action<Color>(t => groupBox.BackColor = t), Color.LightBlue);
                            lock (Form1.locker)
                            {
                                // запись в файл
                                string buildnote = "otherdigit";
                                //string buildchanel = chanel.ToString();
                                string buildend = ".txt";
                                string buildsum = buildnote + buildend;

                                StreamWriter sw;
                                sw = File.AppendText(buildsum);
                                sw.WriteLine(obj.ToString() + "   " + serialPort.PortName);
                                sw.Close();
                                break;
                            }
                            //groupBox.BackColor = Color.LightBlue;                        
                            
                        }
                        else
                        {
                            groupBox.Invoke(new Action<Color>(t => groupBox.BackColor = t), Color.LightGray);
                        }
                    }
                
                    Thread.Sleep(1);
                }
                else
                {
                    Thread.Sleep(50);
                }
            }
        }


  
        static int CRC_16(byte[] bytes, int len, byte flag)//byte[] bytes//int CRC_16(byte* buffer, int len, byte flag)
        {
            int crc = 0xffff;
            int ind = 0;
            while (len > 0)
            {
                crc = crc16_update(crc, bytes[ind]);//crc=crc16_update(crc,*buffer)
                ind++;//buffer++
                len--;
            }
            if (flag > 0)
            {
                crc = crc & 0x0000;
                crc++;
            }
            return crc;
        }

        static int crc16_update(int crc, byte a)
        {
            int i;

            crc ^= a;
            for (i = 0; i < 8; ++i)
            {
                if ((crc & 1) != 0)
                    crc = (crc >> 1) ^ 0xA001;
                else
                    crc = (crc >> 1);
            }

            return crc;
        }
        private void button1_Click(object sender, EventArgs e)//открыть порт
        {
            for (uint i = 0; i < MAXCOM; i++)
            {
                comobj[i].mtxcom.WaitOne();
                if (comobj[i].serialPort.IsOpen) comobj[i].serialPort.Close();
                comobj[i].mtxcom.ReleaseMutex();
            }

         
            for (uint i = 0; i < MAXCOM; i++)
            {


                try
                {
                    comobj[i].mtxcom.WaitOne();
                    comobj[i].serialPort.PortName = comobj[i].listBox.SelectedItem.ToString();
                    comobj[i].serialPort.BaudRate = int.Parse(comobj[i].comboBox.SelectedItem.ToString());//
                    comobj[i].mtxcom.ReleaseMutex();
                    comobj[i].mtxcom.WaitOne();

                    comobj[i].serialPort.Open();
                    comobj[i].labelName.Text = comobj[i].serialPort.PortName;
                    comobj[i].labelBaudrate.Text = comobj[i].serialPort.BaudRate.ToString();

                    comobj[i].groupBox.BackColor = System.Drawing.SystemColors.Control;
                    // объект для сериализации
                    ComSer com = new ComSer(comobj[i].serialPort.PortName, comobj[i].serialPort.BaudRate);

                    string buildnote = "com";
                    string buildchanel = (i+1).ToString();
                    string buildend = "r.xml";
                    string buildsum = buildnote + buildchanel + buildend;
                    // передаем в конструктор тип класса
                    XmlSerializer formatter = new XmlSerializer(typeof(ComSer));

                    // получаем поток, куда будем записывать сериализованный объект
                    using (FileStream fs = new FileStream(@buildsum, FileMode.Truncate))
                    {
                        formatter.Serialize(fs, com);
                    }

                    comobj[i].mtxcom.ReleaseMutex();
                }
                catch (Exception ex)//
                {
                    comobj[i].groupBox.BackColor = Color.LightPink;
                    comobj[i].mtxcom.ReleaseMutex();
                }

            }
        }
        [Serializable]

   
        public class Ip
        {

            public string IpNum { get; set; }
            public int Port { get; set; }
            public int HostNum { get; set; }




            // стандартный конструктор без параметров
            public Ip()
            { }

            public Ip(string ipnum, int port, int hostnum)
            {

                IpNum = ipnum;
                Port = port;
                HostNum = hostnum;

            }
        }

        public class ComSer
        {
            public string Name { get; set; }
            public int Baudrate { get; set; }
            public ComSer()
            {
            }
            public ComSer(string name, int baudrate)
            {
                Name = name;
                Baudrate = baudrate;      
            }
        }


            public class Com
        {


            public System.IO.Ports.SerialPort serialPort;// { get; set; }
            public System.Windows.Forms.Label labelName;// { get; set; }
            public System.Windows.Forms.Label labelBaudrate;// { get; set; }
            public System.Windows.Forms.Label labelTime;// { get; set; }
            public System.Windows.Forms.Label labelMaxTime;// { get; set; }
            public System.Windows.Forms.Label labelNumReq;// { get; set; }
            public System.Windows.Forms.Label labelNumAnsw;// { get; set; }
            public System.Windows.Forms.ComboBox comboBox;// { get; set; }
            public System.Windows.Forms.GroupBox groupBox;// { get; set; }
            public System.Windows.Forms.ListBox listBox;// { get; set; }
            public Mutex mtxcom;// = new Mutex();
            public AutoResetEvent are;// = new AutoResetEvent(false);

            

            // стандартный конструктор без параметров
            public Com()
            {
                    mtxcom = new Mutex();
                    are = new AutoResetEvent(false);
            }
            public Form1 frm;
            public void Polling()
            {
                frm.Func_Polling(mtxcom, serialPort, are, labelNumReq,
                labelNumAnsw, labelTime, labelMaxTime, groupBox);
            }
        
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void timer1_Tick(object sender, EventArgs e)
        {
            for (uint i = 0; i < MAXCOM; i++)
            {
                comobj[i].are.Set();
            }         
        }

  


    }
}
